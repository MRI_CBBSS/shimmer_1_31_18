/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package com.shimmerresearch.multishimmertemplate;

public final class R {
  public static final class array {
    public static final int nav_drawer_icons=0x7f010000;
    public static final int options_menu=0x7f010001;
    public static final int plotColor=0x7f010002;
    public static final int sensors_to_heart_rate=0x7f010003;
    public static final int sensors_to_heart_rate_ecg=0x7f010004;
  }
  public static final class color {
    public static final int app_background_color=0x7f020000;
    public static final int black=0x7f020001;
    public static final int blue=0x7f020002;
    public static final int brown=0x7f020003;
    public static final int counter_text_bg=0x7f020004;
    public static final int counter_text_color=0x7f020005;
    public static final int dark_blue=0x7f020006;
    public static final int dark_gray=0x7f020007;
    public static final int dark_green=0x7f020008;
    public static final int dark_orange=0x7f020009;
    public static final int dark_red=0x7f02000a;
    public static final int dialog_background_color=0x7f02000b;
    public static final int edit_text_color=0x7f02000c;
    public static final int gold=0x7f02000d;
    public static final int gray=0x7f02000e;
    public static final int green=0x7f02000f;
    public static final int green_circle=0x7f020010;
    public static final int indigo=0x7f020011;
    public static final int lightgrey=0x7f020012;
    public static final int lightgrey2=0x7f020013;
    public static final int lightgrey3=0x7f020014;
    public static final int lime=0x7f020015;
    public static final int list_background=0x7f020016;
    public static final int list_background_disabled=0x7f020017;
    public static final int list_background_pressed=0x7f020018;
    public static final int list_divider=0x7f020019;
    public static final int list_item_title=0x7f02001a;
    public static final int orange=0x7f02001b;
    public static final int pink=0x7f02001c;
    public static final int purple=0x7f02001d;
    public static final int red=0x7f02001e;
    public static final int red_circle=0x7f02001f;
    public static final int shimmer_blue=0x7f020020;
    public static final int shimmer_grey=0x7f020021;
    public static final int shimmer_orange=0x7f020022;
    public static final int text_color=0x7f020023;
    public static final int text_color_light=0x7f020024;
    public static final int turquoise=0x7f020025;
    public static final int violet=0x7f020026;
    public static final int yellow=0x7f020027;
    public static final int yellow_circle=0x7f020028;
  }
  public static final class dimen {
    public static final int large=0x7f030000;
    public static final int medium=0x7f030001;
    public static final int small=0x7f030002;
  }
  public static final class drawable {
    public static final int button_gray_pressed=0x7f040000;
    public static final int button_grey=0x7f040001;
    public static final int button_orange=0x7f040002;
    public static final int button_orange_pressed=0x7f040003;
    public static final int circle_blue=0x7f040004;
    public static final int circle_green=0x7f040005;
    public static final int circle_red=0x7f040006;
    public static final int circle_yellow=0x7f040007;
    public static final int ic_action_forward=0x7f040008;
    public static final int ic_action_overflow=0x7f040009;
    public static final int ic_action_stop_write=0x7f04000a;
    public static final int ic_action_write=0x7f04000b;
    public static final int ic_blank=0x7f04000c;
    public static final int ic_drawer=0x7f04000d;
    public static final int ic_heart=0x7f04000e;
    public static final int ic_home=0x7f04000f;
    public static final int ic_launcher=0x7f040010;
    public static final int ic_play=0x7f040011;
    public static final int ic_plot=0x7f040012;
    public static final int ic_settings=0x7f040013;
    public static final int ic_shimmer=0x7f040014;
    public static final int ic_stop=0x7f040015;
    public static final int list_item_bg_normal=0x7f040016;
    public static final int list_item_bg_pressed=0x7f040017;
    public static final int list_selector=0x7f040018;
    public static final int selector_grey=0x7f040019;
    public static final int selector_image_menu=0x7f04001a;
    public static final int selector_listdevices=0x7f04001b;
    public static final int selector_orange=0x7f04001c;
    public static final int shimmer_icon=0x7f04001d;
  }
  public static final class id {
    public static final int ButtonNo=0x7f050000;
    public static final int ButtonYes=0x7f050001;
    public static final int CHILD_ID=0x7f050002;
    public static final int CheckBoxIntExpPow=0x7f050003;
    public static final int CheckBoxPPGToHeartRate=0x7f050004;
    public static final int ConfigurationButton=0x7f050005;
    public static final int ConnectButton=0x7f050006;
    public static final int DeleteButton=0x7f050007;
    public static final int Enable_Event=0x7f050008;
    public static final int PARENT_ID=0x7f050009;
    public static final int SensorProfileButton=0x7f05000a;
    public static final int Start_Stats=0x7f05000b;
    public static final int Stream_Stored=0x7f05000c;
    public static final int StreamingButton=0x7f05000d;
    public static final int ToggleLedButton=0x7f05000e;
    public static final int accelRange=0x7f05000f;
    public static final int battVal=0x7f050010;
    public static final int bluetooth_address=0x7f050011;
    public static final int buttonAccel=0x7f050012;
    public static final int buttonBattLimit=0x7f050013;
    public static final int buttonCancelHearRate=0x7f050014;
    public static final int buttonDeviceName=0x7f050015;
    public static final int buttonDone=0x7f050016;
    public static final int buttonDoneHearRate=0x7f050017;
    public static final int buttonEnableSensor=0x7f050018;
    public static final int buttonEnableSensors=0x7f050019;
    public static final int buttonEnableSensorsConfiguration=0x7f05001a;
    public static final int buttonExgGain=0x7f05001b;
    public static final int buttonExgRes=0x7f05001c;
    public static final int buttonGSR=0x7f05001d;
    public static final int buttonGyroRange=0x7f05001e;
    public static final int buttonMagRange=0x7f05001f;
    public static final int buttonPlotDone=0x7f050020;
    public static final int buttonPressureAccuracy=0x7f050021;
    public static final int buttonRate=0x7f050022;
    public static final int buttonSave=0x7f050023;
    public static final int buttonSelectSensor=0x7f050024;
    public static final int buttonSensorStreamDone=0x7f050025;
    public static final int buttonSetFileName=0x7f050026;
    public static final int buttonStartWriteData=0x7f050027;
    public static final int button_invalid_name=0x7f050028;
    public static final int button_scan=0x7f050029;
    public static final int checkBox5VReg=0x7f05002a;
    public static final int checkBoxLowPowerAccel=0x7f05002b;
    public static final int checkBoxLowPowerGyro=0x7f05002c;
    public static final int checkBoxLowPowerMag=0x7f05002d;
    public static final int checkBoxSensor=0x7f05002e;
    public static final int checkBoxSignal=0x7f05002f;
    public static final int content_frame=0x7f050030;
    public static final int deviceName=0x7f050031;
    public static final int device_name=0x7f050032;
    public static final int drawer_layout=0x7f050033;
    public static final int dynamicPlot=0x7f050034;
    public static final int editTextConfigName=0x7f050035;
    public static final int editTextNumberOfBeats=0x7f050036;
    public static final int floating_heart=0x7f050037;
    public static final int floating_text=0x7f050038;
    public static final int heart_rate_layout=0x7f050039;
    public static final int help=0x7f05003a;
    public static final int icon=0x7f05003b;
    public static final int image_menu=0x7f05003c;
    public static final int image_state=0x7f05003d;
    public static final int lateral_bar=0x7f05003e;
    public static final int lateral_bar3=0x7f05003f;
    public static final int layoutButton=0x7f050040;
    public static final int layoutButtonPlotDone=0x7f050041;
    public static final int layoutNewDevices=0x7f050042;
    public static final int layoutPairedDevices=0x7f050043;
    public static final int layout_heart_rate=0x7f050044;
    public static final int left_drawer=0x7f050045;
    public static final int linearlayout=0x7f050046;
    public static final int listDevices=0x7f050047;
    public static final int listEnableSensor=0x7f050048;
    public static final int listPlotDevices=0x7f050049;
    public static final int listPlotSensors=0x7f05004a;
    public static final int new_devices=0x7f05004b;
    public static final int paired_devices=0x7f05004c;
    public static final int plotStatsButton=0x7f05004d;
    public static final int rowTextView=0x7f05004e;
    public static final int samplingRate=0x7f05004f;
    public static final int sensorProfileBack=0x7f050050;
    public static final int sensorText=0x7f050051;
    public static final int serverStreamButton=0x7f050052;
    public static final int settings=0x7f050053;
    public static final int spinnerSensors=0x7f050054;
    public static final int streamDeviceName=0x7f050055;
    public static final int streamSensorsList=0x7f050056;
    public static final int textNumberOfBeats=0x7f050057;
    public static final int textSelectSensors=0x7f050058;
    public static final int textSignal=0x7f050059;
    public static final int textView1=0x7f05005a;
    public static final int textViewConfigurationTitle=0x7f05005b;
    public static final int textViewDataReceived=0x7f05005c;
    public static final int textViewDialog=0x7f05005d;
    public static final int textViewEnableTitle=0x7f05005e;
    public static final int textViewSensorsTitle=0x7f05005f;
    public static final int text_invalid_name=0x7f050060;
    public static final int title_menu_option=0x7f050061;
    public static final int title_new_devices=0x7f050062;
    public static final int title_paired_devices=0x7f050063;
    public static final int write=0x7f050064;
  }
  public static final class layout {
    public static final int activity_main=0x7f060000;
    public static final int attribute_name=0x7f060001;
    public static final int blank_main=0x7f060002;
    public static final int configuration_fragment=0x7f060003;
    public static final int device_list=0x7f060004;
    public static final int device_name=0x7f060005;
    public static final int devices_fragment=0x7f060006;
    public static final int devices_fragment_dialog=0x7f060007;
    public static final int devices_fragment_item=0x7f060008;
    public static final int dialog_enable_sensor_view=0x7f060009;
    public static final int drawer_list_item=0x7f06000a;
    public static final int enable_sensor_list=0x7f06000b;
    public static final int heart_rate_dialog=0x7f06000c;
    public static final int invalid_name_dialog=0x7f06000d;
    public static final int log_popup=0x7f06000e;
    public static final int plot_main=0x7f06000f;
    public static final int plot_sensor_item=0x7f060010;
    public static final int plot_sensor_list=0x7f060011;
    public static final int plot_sensors_selection=0x7f060012;
    public static final int save_dialog=0x7f060013;
    public static final int sensor_profile=0x7f060014;
    public static final int server_stream_list=0x7f060015;
    public static final int server_stream_sensor_item=0x7f060016;
    public static final int text_dialog=0x7f060017;
    public static final int write_data_menu=0x7f060018;
  }
  public static final class menu {
    public static final int activity_main=0x7f070000;
    public static final int activity_main_plot=0x7f070001;
    public static final int popup=0x7f070002;
  }
  public static final class string {
    public static final int app_name=0x7f080000;
    public static final int button_scan=0x7f080001;
    public static final int configure_device=0x7f080002;
    public static final int drawer_close=0x7f080003;
    public static final int drawer_open=0x7f080004;
    public static final int enable_sensors_done=0x7f080005;
    public static final int main=0x7f080006;
    public static final int sensor_prompt=0x7f080007;
    public static final int title_other_devices=0x7f080008;
    public static final int title_paired_devices=0x7f080009;
  }
  public static final class style {
    public static final int AppBaseTheme=0x7f090000;
    public static final int AppTheme=0x7f090001;
    public static final int GroupMiniText=0x7f090002;
    public static final int GroupTitleText=0x7f090003;
  }
}