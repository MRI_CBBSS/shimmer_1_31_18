package com.shimmerresearch.multishimmertemplate;

/*
* Button to start streaming
* choose sensors for streaming from enabled sensors
*
* PROBLEM: SensorProfile is only accessible thought Devices Fragment WHEN ITS NOT STREAMING!!
* TODO: find a way to access sensor profile from plot fragment when sensor is streaming!! -> Will become stats dialog in plot fragment
*
* NOT STREAMING: Stored data to server
* WHEN STREAMING:
* */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.common.collect.BiMap;
import com.shimmerresearch.adapters.CheckboxListAdapter;
import com.shimmerresearch.android.Shimmer;
import com.shimmerresearch.database.DatabaseHandler;
import com.shimmerresearch.database.ShimmerConfiguration;
import com.shimmerresearch.driver.Configuration;
import com.shimmerresearch.driver.ShimmerObject;
import com.shimmerresearch.driver.ShimmerVerDetails;
import com.shimmerresearch.driver.Configuration.Shimmer3;
import com.shimmerresearch.service.MultiShimmerTemplateService;

import java.util.Arrays;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;

import org.w3c.dom.Text;

public class SensorProfile extends Fragment{

    public View rootView = null;
    public MultiShimmerTemplateService mService;
    DatabaseHandler db;

    public static String mDone = "Done";
    String mAttribute;
    String deviceBluetoothAddress;
    String[] mValues={""};
    double mAttributeValue;
    double mShimmerVersion;
    TextView mTVAttribute;
    CheckBox mChechBox;
    int currentPosition;
    ShimmerConfiguration shimmerConfig;
    public Dialog enableSensorDialog;
    public Dialog heartRateDialog;
    public ListView enableSensorListView;
    public long enabledSensors;
    public int exgRes;
    public String [] compatibleSensors;
    public BiMap<String,String> sensorBitmaptoName=null;
    public String mSensorToHeartRate;
    public boolean modifyHRState = true;

    TextView deviceName;
    TextView samplingRateText;
    TextView accelRange;
    TextView battVal;
    Button serverStreamButton;
    Button buttonSensorStreamDone;
    Button sensorProfileBack;
    Button streamStoredData;
    Button enableStatistics;
    Button enableEvent;
    Dialog serverStreamDialog;
    CheckBox cBoxLowPowerAccel;
    ListView streamDevicesList;
    TextView streamDeviceName;
    ListView streamSensorsList;
    View lateral_bar3;
    public int lastDeviceSelected=-1;
    static String[] deviceNames;
    public HashMap<String, List<SelectedSensors>> mSelectedSensors = new HashMap<String, List<SelectedSensors>>(7);
    String[] deviceBluetoothAddresses;






    String mBluetoothAddress;


    public SensorProfile() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//		getActivity().invalidateOptionsMenu();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.sensor_profile, container, false);

        this.mService = ((MainActivity)getActivity()).mService;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if(mService!=null){
            setup();
        }

        //get the position of the device in the listView
        Bundle arg = getArguments();
        deviceBluetoothAddress = arg.getString("address");
        currentPosition = arg.getInt("position");
        currentPosition-=2;
        shimmerConfig = mService.mShimmerConfigurationList.get(currentPosition);

        double mSamplingRateV = shimmerConfig.getSamplingRate();
        int mAccelerometerRangeV = shimmerConfig.getAccelRange();
        int mGSRRangeV = shimmerConfig.getGSRRange();
//    	final double batteryLimit = shimmerConfig.; there's nothing for the battery??
        final String[] samplingRate = new String [] {"8","16","51.2","102.4","128","204.8","256","512","1024","2048"};
        final String[] exgGain = new String [] {"6","1","2","3","4","8","12"};
        final String[] exgResolution = new String [] {"16 bits","24 bits"};

        deviceName = (TextView) rootView.findViewById(R.id.deviceName);
        samplingRateText = (TextView) rootView.findViewById(R.id.samplingRate);
        accelRange = (TextView) rootView.findViewById(R.id.accelRange);
        battVal = (TextView) rootView.findViewById(R.id.battVal);
        sensorProfileBack = (Button) rootView.findViewById(R.id.sensorProfileBack);
        /******************************************************************************************/
        // Set an EditText view to get user input
        final EditText editTextBattLimit = new EditText(getActivity());
        final EditText editTextDeviceName = new EditText(getActivity());
        editTextBattLimit.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);


        mSensorToHeartRate="";
        final String[] accelRangeArray = {"+/- 1.5g","+/- 6g"};
        mBluetoothAddress = shimmerConfig.getBluetoothAddress();
        String rate = Double.toString(mSamplingRateV);
        samplingRateText.setText("Sampling Rate "+"\n ("+rate+" Hz)");
        deviceName.setText("Device Name"+"\n ("+shimmerConfig.getDeviceName()+")");
        accelRange.setText("Acceleromete Range" + "\n ("+mAccelerometerRangeV+" g)");
        battVal.setText("Battery: " + "\n (" +Shimmer.SENSOR_BATT+") ");

        sensorProfileBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
                Fragment fragment = new DevicesFragment();
                FragmentManager fragmentManager = getFragmentManager();
                String tag = "Devices";
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment, tag).commit();
                getActivity().getActionBar().setTitle(tag); //set the title of the window
            }
        });


        serverStreamDialog = new Dialog(getActivity());
        serverStreamDialog.setTitle("Stream to Server");
        serverStreamDialog.setContentView(R.layout.server_stream_list);

        //streamDevicesList = (ListView) serverStreamDialog.findViewById(R.id.streamDevicesList);
        //streamDevicesList.setChoiceMode(1);
        //streamDevicesList.setItemChecked(0, true);
        streamDeviceName = (TextView) serverStreamDialog.findViewById(R.id.streamDeviceName);
        streamDeviceName.setText("      Sensor: " + shimmerConfig.getDeviceName() + "       ");


        streamSensorsList = (ListView) serverStreamDialog.findViewById(R.id.streamSensorsList);
        lateral_bar3 = (View) serverStreamDialog.findViewById(R.id.lateral_bar3);

        serverStreamButton = (Button) rootView.findViewById(R.id.serverStreamButton);

        serverStreamButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //final String[] planets = new String[] { "Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"};
                //final ArrayList<String> planetList = new ArrayList<String>();
                //planetList.addAll( Arrays.asList(planets) );
                //ArrayAdapter<String> adapterSensors = new ArrayAdapter<String>(getActivity(), R.layout.server_stream_sensor_item, planetList);
                //ArrayAdapter<String> adapterDevices = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                //adapterDevices.addAll(deviceNames);
                //FIXME add devices to list -> remove sample
                //streamSensorsList.setAdapter( adapterSensors );

                serverStreamDialog.show();

                //start streaming
                mService.startWebStreaming(deviceBluetoothAddress);
            }
        });

        //two new buttons
        streamStoredData = (Button) rootView.findViewById(R.id.Stream_Stored);

        streamStoredData.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mService.streamStored(deviceBluetoothAddress);
            }

        });

        enableStatistics = (Button) rootView.findViewById(R.id.Start_Stats);

        enableStatistics.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mService.enableStats(deviceBluetoothAddress);

            }

        });

        enableEvent = (Button) rootView.findViewById(R.id.Enable_Event);

        enableEvent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mService.enableEvent(deviceBluetoothAddress);

            }

        });

        /*streamDevicesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int position, long id) {
                // TODO Auto-generated method stub
                //FIXME

            }
        });
        */

        buttonSensorStreamDone = (Button) serverStreamDialog.findViewById(R.id.buttonSensorStreamDone);
        buttonSensorStreamDone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                serverStreamDialog.dismiss();
            }
        });


        return rootView;

    }

    public void onPause(){
        super.onPause();
        if (mService!=null){
            db.saveShimmerConfigurations("Temp", mService.mShimmerConfigurationList);
        }

    }

    public void setup(){
        db=mService.mDataBase;
        mService.enableGraphingHandler(false);
//	  	mService.setHandler(mHandler);
        mService.mShimmerConfigurationList = db.getShimmerConfigurations("Temp");
    }
}
