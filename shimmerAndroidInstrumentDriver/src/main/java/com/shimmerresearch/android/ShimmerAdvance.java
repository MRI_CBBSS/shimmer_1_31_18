package com.shimmerresearch.android;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.shimmerresearch.bluetooth.ShimmerBluetooth;
import com.shimmerresearch.driver.FormatCluster;
import com.shimmerresearch.driver.ObjectCluster;
import com.shimmerresearch.tools.Logging;
import com.shimmerresearch.webSockets.WebSocket;
import com.shimmerresearch.webSockets.WebSocketAdapter;
import com.shimmerresearch.webSockets.WebSocketException;
import com.shimmerresearch.webSockets.WebSocketExtension;
import com.shimmerresearch.webSockets.WebSocketFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import static java.lang.Double.parseDouble;


/**
 * Created by mingli-student on 2/6/2018.
 */


public class ShimmerAdvance extends Shimmer implements ShimmerBluetooth.DataProcessing {

    //statistics names
    public static final String MEAN = "MEAN";
    public static final String VARIANCE = "VARIANCE";
    public static final String STANDARD_DEVIATION = "STANDARD_DEVIATION";
    public static final String MEDIAN = "MEDIAN";
    public static final String IQR = "IQR";
    public static final String MAX = "MAX";
    public static final String MIN = "MIN";


    //event names
    public static final String ACCEL_TOO_FAST = "ACCEL_TOO_FAST";

    //analysis names
    public static final String EVERY_HUN_AVERAGE = "EVERY_HUN_AVERAGE";



    //**********************************************************************************************
    //***** Class Variables and Constructors
    //**********************************************************************************************

    boolean  mWebSocketStreaming = false;
    boolean  mStatisticsEnabled  = false;
    boolean  mEventListEmpty     = true;
    boolean  mIsArcLog           = false;
    boolean  mIsTempLog          = false;
    boolean  mIsUnsLog           = false;
    boolean  mIntConnected       = false;
    boolean  mWSConected         = false;

    //boolean  mLogStats           = false;
    HashMap<String, sensorBuffer> mSenBufferMap = new HashMap<String, sensorBuffer>(this.MAX_NUMBER_OF_SIGNALS);
    HashMap<String, StatisticsManager> mStatisticsMap = new HashMap<>();
    HashMap<String, String[]> mEventDetMap = new HashMap<String, String[]>();
    AnalysisManager mAnalysisManager;
    Logging mUnsentLog;
    Logging mTempLog;
    public Logging mArchiveLog;
    String mLoggerFileName = "Data";
    //File            mStatsFile;
    //BufferedWriter  mStatsWriter;
    double mStartTime = 0;
    int mArchiveCounter = 0;
    int mStatsCounter = 0;
    final int ArchiveSize = 1000;

    /**
     * The timeout value in milliseconds for socket connection.
     */
    private static final int TIMEOUT = 5000;
    WebSocket ws;
    WebSocket mPostStream;
    String serverMsg;
    //fresno state server
    private static final String SERVER = "ws://129.8.242.45:8080";


    //Constructors from superclass Shimmer all imported
    /**
     * Constructor. Prepares a new Bluetooth session.
     * @param context  The UI Activity Context
     * @param handler  A Handler to send messages back to the UI Activity
     * @param myname  To allow the user to set a unique identifier for each Shimmer device
     * @param countiousSync A boolean value defining whether received packets should be checked continuously for the correct start and end of packet.
     */
    public ShimmerAdvance(Handler handler, String myName, Boolean continousSync) {

        super(handler, myName, continousSync);
        setDataProcessing((DataProcessing) this);
    }

    public ShimmerAdvance(Context context, Handler handler, String myName, Boolean continousSync) {

        super(context, handler, myName, continousSync);
        setDataProcessing((DataProcessing) this);
    }

    /**
     * Constructor. Prepares a new Bluetooth session. Additional fields allows the device to be set up immediately.
     * @param context  The UI Activity Context
     * @param handler  A Handler to send messages back to the UI Activity
     * @param myname  To allow the user to set a unique identifier for each Shimmer device
     * @param samplingRate Defines the sampling rate
     * @param accelRange Defines the Acceleration range. Valid range setting values for the Shimmer 2 are 0 (+/- 1.5g), 1 (+/- 2g), 2 (+/- 4g) and 3 (+/- 6g). Valid range setting values for the Shimmer 2r are 0 (+/- 1.5g) and 3 (+/- 6g).
     * @param gsrRange Numeric value defining the desired gsr range. Valid range settings are 0 (10kOhm to 56kOhm),  1 (56kOhm to 220kOhm), 2 (220kOhm to 680kOhm), 3 (680kOhm to 4.7MOhm) and 4 (Auto Range).
     * @param setEnabledSensors Defines the sensors to be enabled (e.g. 'Shimmer.SENSOR_ACCEL|Shimmer.SENSOR_GYRO' enables the Accelerometer and Gyroscope)
     * @param countiousSync A boolean value defining whether received packets should be checked continuously for the correct start and end of packet.
     */
    public ShimmerAdvance(Context context, Handler handler, String myName, double samplingRate, int accelRange, int gsrRange, long setEnabledSensors, boolean continousSync) {

        super(context, handler, myName, samplingRate, accelRange, gsrRange, setEnabledSensors, continousSync);
        setDataProcessing((DataProcessing) this);
    }

    /**
     * Constructor. Prepares a new Bluetooth session. Additional fields allows the device to be set up immediately.
     * @param context  The UI Activity Context
     * @param handler  A Handler to send messages back to the UI Activity
     * @param myname  To allow the user to set a unique identifier for each Shimmer device
     * @param samplingRate Defines the sampling rate
     * @param accelRange Defines the Acceleration range. Valid range setting values for the Shimmer 2 are 0 (+/- 1.5g), 1 (+/- 2g), 2 (+/- 4g) and 3 (+/- 6g). Valid range setting values for the Shimmer 2r are 0 (+/- 1.5g) and 3 (+/- 6g).
     * @param gsrRange Numeric value defining the desired gsr range. Valid range settings are 0 (10kOhm to 56kOhm),  1 (56kOhm to 220kOhm), 2 (220kOhm to 680kOhm), 3 (680kOhm to 4.7MOhm) and 4 (Auto Range).
     * @param setEnabledSensors Defines the sensors to be enabled (e.g. 'Shimmer.SENSOR_ACCEL|Shimmer.SENSOR_GYRO' enables the Accelerometer and Gyroscope)
     * @param countiousSync A boolean value defining whether received packets should be checked continuously for the correct start and end of packet.
     */
    public ShimmerAdvance(Context context, Handler handler, String myName, double samplingRate, int accelRange, int gsrRange, long setEnabledSensors, boolean continousSync, int magGain) {

        super(context, handler, myName, samplingRate, accelRange, gsrRange, setEnabledSensors, continousSync, magGain);
        setDataProcessing((DataProcessing) this);
    }


    /**
     * Constructor. Prepares a new Bluetooth session. Additional fields allows the device to be set up immediately.
     * @param context  The UI Activity Context
     * @param handler  A Handler to send messages back to the UI Activity
     * @param myname  To allow the user to set a unique identifier for each Shimmer device
     * @param samplingRate Defines the sampling rate
     * @param accelRange Defines the Acceleration range. Valid range setting values for the Shimmer 2 are 0 (+/- 1.5g), 1 (+/- 2g), 2 (+/- 4g) and 3 (+/- 6g). Valid range setting values for the Shimmer 2r are 0 (+/- 1.5g) and 3 (+/- 6g).
     * @param gsrRange Numeric value defining the desired gsr range. Valid range settings are 0 (10kOhm to 56kOhm),  1 (56kOhm to 220kOhm), 2 (220kOhm to 680kOhm), 3 (680kOhm to 4.7MOhm) and 4 (Auto Range).
     * @param setEnabledSensors Defines the sensors to be enabled (e.g. 'Shimmer.SENSOR_ACCEL|Shimmer.SENSOR_GYRO' enables the Accelerometer and Gyroscope)
     * @param countiousSync A boolean value defining whether received packets should be checked continuously for the correct start and end of packet.
     */
    public ShimmerAdvance(Context context, Handler handler, String myName, double samplingRate, int accelRange, int gsrRange, long setEnabledSensors, boolean continousSync, boolean enableLowPowerAccel, boolean enableLowPowerGyro, boolean enableLowPowerMag, int gyroRange, int magRange) {

        super(context, handler, myName, samplingRate, accelRange, gsrRange, setEnabledSensors, continousSync, enableLowPowerAccel, enableLowPowerGyro, enableLowPowerMag, gyroRange, magRange);
        setDataProcessing((DataProcessing) this);
    }


    /**
     * Constructor. Prepares a new Bluetooth session. Additional fields allows the device to be set up immediately.
     * @param context  The UI Activity Context
     * @param handler  A Handler to send messages back to the UI Activity
     * @param myname  To allow the user to set a unique identifier for each Shimmer device
     * @param samplingRate Defines the sampling rate
     * @param accelRange Defines the Acceleration range. Valid range setting values for the Shimmer 2 are 0 (+/- 1.5g), 1 (+/- 2g), 2 (+/- 4g) and 3 (+/- 6g). Valid range setting values for the Shimmer 2r are 0 (+/- 1.5g) and 3 (+/- 6g).
     * @param gsrRange Numeric value defining the desired gsr range. Valid range settings are 0 (10kOhm to 56kOhm),  1 (56kOhm to 220kOhm), 2 (220kOhm to 680kOhm), 3 (680kOhm to 4.7MOhm) and 4 (Auto Range).
     * @param setEnabledSensors Defines the sensors to be enabled (e.g. 'Shimmer.SENSOR_ACCEL|Shimmer.SENSOR_GYRO' enables the Accelerometer and Gyroscope)
     * @param countiousSync A boolean value defining whether received packets should be checked continuously for the correct start and end of packet.
     */
    public ShimmerAdvance(Context context, Handler handler, String myName, double samplingRate, int accelRange, int gsrRange, long setEnabledSensors, boolean continousSync, boolean enableLowPowerAccel, boolean enableLowPowerGyro, boolean enableLowPowerMag, int gyroRange, int magRange, byte[] exg1, byte[] exg2) {

        super(context, handler, myName, samplingRate, accelRange, gsrRange, setEnabledSensors, continousSync, enableLowPowerAccel, enableLowPowerGyro, enableLowPowerMag, gyroRange, magRange, exg1, exg2);
        setDataProcessing((DataProcessing) this);
    }


    //Implementing Data Processing from ShimmerBluetooth Super Class called after the BuldMsg function has formatted the object cluster
    // Initialise Process Data here. This is called whenever the startStreaming command is called and can be used to initialise algorithms
    //initialize mArchiveLog in order to log data when the first object cluster is received
    @Override
    public void InitializeProcessData(){

        mStartTime = System.currentTimeMillis();
        mArchiveLog = new Logging(fromMilisecToDate(System.currentTimeMillis()) + "Archive" + this.getBluetoothAddress(), ",", "TestShimmer");
        mIsArcLog = true;
    }

    /** Process data here, algorithms can access the object cluster built by the buildMsg method here
     * @param ojc the objectCluster built by the buildMsg method
     * @return the processed objectCluster
     */
    @Override
    public ObjectCluster ProcessData(ObjectCluster ojc){

        String[] sensorNames;
        String   format;
        double   timeStamp = 0;

        //if (mTempLog == null) {
            //initialize logger with current time, and folder name
        //    mTempLog = new Logging(fromMilisecToDate(System.currentTimeMillis()) + "temp" + this.getBluetoothAddress(), ",", "TestShimmer");
        //}

        //log data
        if(mIsArcLog) {
            mArchiveLog.logData(ojc);
            mArchiveCounter++;
            if (mArchiveCounter >= ArchiveSize) {
                mIsArcLog = false;
            }
        }
        //if (mIsTempLog) {
        //    mTempLog.logData(ojc);
        //}
        if (!isInternetAvailable()) {
            if (mUnsentLog == null || !mIsUnsLog) {
                mUnsentLog = new Logging(fromMilisecToDate(System.currentTimeMillis()) + "Unsent" + this.getBluetoothAddress(), ",", "TestShimmer");
                mIsUnsLog = true;
            }
            mUnsentLog.logData(ojc);
            mWSConected = false;
        }
        else if (mWebSocketStreaming && !mWSConected ) {
            try {
                ws = connect();
            } catch (Exception WebSocketException) {}
            if (ws != null && ws.isOpen())
                mWSConected = true;
            if (mIsUnsLog) {
                streamStoredThread st = new streamStoredThread(mUnsentLog);
                st.run();
                mIsUnsLog = false;
                mUnsentLog = null;
            }
        }

        //start streaming the data from the object cluster
        format = "CAL";
        if ((mWebSocketStreaming && mWSConected) || mStatisticsEnabled) {
            ByteBuffer wrapped = ByteBuffer.wrap(ojc.mSystemTimeStamp);
            timeStamp = wrapped.getDouble();

            //send the time for all samples in this object cluster
            if (mWebSocketStreaming && mWSConected && ws.isOpen()) {
                ws.sendText("Time");
                ws.sendText(String.valueOf(timeStamp));
                ws.sendText("milisec");
            }

            //loop through all sensors in the object cluster, only considering the CAL formatted data
            sensorNames = ojc.mSensorNames;
            for (int i = 0; i < sensorNames.length; i++)
            {
                if (sensorNames[i] != "") {
                    Collection<FormatCluster> formats = ojc.mPropertyCluster.get(sensorNames[i]);
                    //format = CAL, using calibrated data
                    FormatCluster formatCluster = ((FormatCluster) ojc.returnFormatCluster(formats, format));
                    //have a format cluster of CAL type for sensorNames[i]
                    if (formatCluster != null) {
                        double temp = formatCluster.mData;
                        //send data to the server
                        if (mWebSocketStreaming && mWSConected && ws.isOpen()) {
                            ws.sendText(sensorNames[i]); //server receives a triple of data (type, data, units)
                            ws.sendText(String.valueOf(temp));
                            ws.sendText(formatCluster.mUnits);
                        }
                        //if the sensor does not have a buffer, allocate it one
                        //store the current data, and initialize event detection for this sensor
                        if (!mSenBufferMap.containsKey(sensorNames[i])) {
                            //initialize sesnor buffer for this sensor (first time showing up in object cluster)
                            mSenBufferMap.put(sensorNames[i], new sensorBuffer(sensorNames[i], format, formatCluster.mUnits));
                            sensorBuffer buff = mSenBufferMap.get(sensorNames[i]);
                            buff.addData(sensorNames[i], format, formatCluster.mData, timeStamp);

                            //initialize event detection
                            if (!mEventListEmpty) {
                                if (mEventDetMap.containsKey(sensorNames[i])) {
                                    String[] events = mEventDetMap.get(sensorNames[i]);
                                    buff.setEventList(events);
                                    mEventDetMap.remove(sensorNames[i]);
                                }
                                if (mEventDetMap.isEmpty())
                                    mEventListEmpty = true;
                            }
                        }
                        else {
                            //store data in the sensor Buffer if it exists in the map
                            sensorBuffer buff = mSenBufferMap.get(sensorNames[i]);
                            buff.addData(sensorNames[i], format, formatCluster.mData, timeStamp);

                            //run event detection
                            if (buff.mEventDetection) {
                                boolean isTrue;
                                for (int k = 0; k < buff.mEventList.size(); k++) {
                                    String event = buff.mEventList.get(k);
                                    if (event == ACCEL_TOO_FAST) {
                                        isTrue = accelTooFast(formatCluster.mData);
                                        if (isTrue) {
                                            if (ojc.mEventDetectionList == null)
                                                ojc.mEventDetectionList = new ArrayList<String>();
                                            ojc.mEventDetectionList.add(sensorNames[i] + " " + ACCEL_TOO_FAST);
                                        }
                                    }
                                    //add new case for additional event detection functions here
                                }
                            }
                        }
                        //preform statistical analysis and store information in object cluster
                        if (mStatisticsEnabled && mStatsCounter == 0) {
                            if (mStatisticsMap.containsKey(sensorNames[i])) {
                                sensorBuffer buff = mSenBufferMap.get(sensorNames[i]);
                                //check if the buffer is full (has at least 100 data points)
                                if (buff!= null && buff.mFull) {
                                //writing the statistics to a file (variables have been deleted
                                    //try {
                                    //    if (mLogStats)
                                    //        mStatsWriter.write("Time: " + String.valueOf(timeStamp) + ", ");
                                    //} catch (IOException e) {}

                                    HashMap<String, Double> statMap;
                                    StatisticsManager stat = mStatisticsMap.get(sensorNames[i]);
                                    if (stat != null) {
                                        statMap = stat.runStatistic(buff.mSenData, buff.mNextOpen);
                                        //insert statistics results into the format cluster
                                        formatCluster.mStats = statMap;
                                    }

                                //for streaming stats to the server
                                    //ArrayList<StatisticsManager> statsAPIs = mStatisticsMap.get(sensorNames[i]);
                                    //for (int x = 0; x < statsAPIs.size(); x++) {
                                    //    tempStat = statsAPIs.get(i).getStatistic(buff.mSenData, buff.mNextOpen);
                                    //    formatCluster.mStats.put(statsAPIs.get(i).mName, tempStat);
                                    //if (ws.isOpen()) {
                                    //    ws.sendText(sensorNames[i]); //server receives a triple of data (type, data, units)
                                    //    ws.sendText(String.valueOf(tempStat));
                                    //    ws.sendText(stat.mName);
                                    //}
                                    //try {
                                    //    if (mLogStats)
                                    //        mStatsWriter.write(String.valueOf(tempStat) + ", ");
                                    //} catch (IOException e) {}
                                    //}
                                    //try {
                                    //    if (mLogStats)
                                    //        mStatsWriter.newLine();
                                    //} catch (IOException e) {}
                                }
                            }
                        }

                    }
                }
            }
        }

        //stats are calculated every 20 samples
        if (mStatisticsEnabled) mStatsCounter = (mStatsCounter + 1) %20;
        return ojc;
    }


    //**********************************************************************************************
    //* Functions that interact with Service
    //**********************************************************************************************

    /**
     * Connect WebSockets for Streaming of sensor data
     * accessed by a button in the SesnorProfile fragment of the UI
     */
    public void startWebSocketStreaming() {

        try {
            mWebSocketStreaming = true;
            if (isInternetAvailable()) {
                ws = connect();
                if (ws != null && ws.isOpen())
                    mWSConected = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WebSocketException e) {
            e.printStackTrace();
        }
    }

    public void stopWebSocketStreaming() {

        mWebSocketStreaming = false;
        closeWebSocket(ws);
    };

    //called in service in stopStreamingAllDevices function
    //re-initialize all member variables or leave as is?
    public void stopStreamingDevices() {

        closeWebSocket(ws);
        mWSConected = false;
    };

    public void setStatistics(HashMap<String, String[]> statMap) throws IOException {

        //ArrayList<String> stats = new ArrayList<>();
        for (String Key : statMap.keySet()) {
        //ArrayList<StatisticsManager> statsAPIs = new ArrayList<>();
            ArrayList<String> statsList = new ArrayList<>();
            String[] statsArr = statMap.get(Key);
            for (int i = 0; i < statsArr.length; i++) {
                if (statsArr[i] == MEAN && !statsList.contains(MEAN)) {
                    statsList.add(MEAN);
                }
                else if(statsArr[i] == VARIANCE && !statsList.contains(VARIANCE)) {
                    statsList.add(VARIANCE);
                }
                else if(statsArr[i] == STANDARD_DEVIATION && !statsList.contains(STANDARD_DEVIATION)) {
                    statsList.add(STANDARD_DEVIATION);
                }
                else if(statsArr[i] == MEDIAN && !statsList.contains(MEDIAN)) {
                    statsList.add(MEDIAN);
                }
                else if(statsArr[i] == IQR && !statsList.contains(IQR)) {
                    statsList.add(IQR);
                }
                else if(statsArr[i] == MAX && !statsList.contains(MAX)) {
                    statsList.add(MAX);
                }
                else if(statsArr[i] == MIN && !statsList.contains(MIN)) {
                    statsList.add(MIN);
                }
                //add statistics here
            }
            if (!statsList.isEmpty())
                mStatisticsMap.put(Key, new StatisticsManager(statsList));
        }

        if (!mStatisticsMap.isEmpty()) mStatisticsEnabled = true;

        //set up logging of statistics
            //File root = new File(Environment.getExternalStorageDirectory() + "/"+"Statistics");

            //if(!root.exists())
            //{
            //    if(root.mkdir()); //directory is created;
            //}
            //changed to text file as opposed to .dat
            // outputFile = new File(root, mFileName+".dat");
            //String fileName = ShimmerAdvance.fromMilisecToDate(System.currentTimeMillis()) + "Analysis" + this.getBluetoothAddress();

            //mStatsFile = new File(root, fileName+".txt");
            //log analysis
            //mStatsWriter = new BufferedWriter(new FileWriter(mStatsFile));
            //mLogStats = true;
            //for (int i = 0; i < stats.size(); i++)
            //    mStatsWriter.write(stats.get(i) + ", ");
            //mStatsWriter.newLine();
    }

    public void stopStatistics() {

        mStatisticsMap = null;
        mStatisticsEnabled = false;
    }

    //events are initialized for each sensor buffer when data is received from the format cluster
    public void setEventDetection(HashMap<String, String[]> senEvents) {

        mEventDetMap = senEvents;

        if (!mEventDetMap.isEmpty())
            mEventListEmpty = false;
    }

    public void setAnalysis(HashMap<String, String[]> anaList) {

        mAnalysisManager = new AnalysisManager(anaList, this.getSamplingRate());
    }

    public HashMap<String, double[][]> startAnalysis () {

        if (mAnalysisManager == null) return null;

        return mAnalysisManager.getAnalysisResults(this);
    }

    public void startStreamingStoredData(Logging logger) {

        //test analysis
            //HashMap<String, String[]> tempStuff = new HashMap<>();
            //String[] tempMoreStuff = new String[1];
            //tempMoreStuff[0] = EVERY_HUN_AVERAGE;
            //tempStuff.put("Low Noise Accelerometer X", tempMoreStuff);
            //setAnalysis(tempStuff);
            //startAnalysis();

            //return;

        if (logger == null)
            return;

        try {
            WebSocket tempStream;
            tempStream = connect();

            File outputFile = logger.getOutputFile();
            BufferedReader reader = new BufferedReader(new FileReader(outputFile));

            String line = null;
            String[] senNames = logger.mSensorNames;
            String[] senUnits = logger.mSensorUnits;

            line = reader.readLine();

            //get to the data
            while ((!line.startsWith("START_DATA_LOG") && (line != null))) {line = reader.readLine();}

            //prepare server for data streaming from this file
            while ((line = reader.readLine()) != null) {
                //take apart the string
                double[] data = fromLineToData(line, logger);

                //stream the data to the server
                for (int i = 0; i < data.length; i++) {
                    if (senNames.length <= i || senUnits.length <= i) continue;
                    if (tempStream != null && tempStream.isOpen()) {
                        tempStream.sendText(senNames[i]);
                        tempStream.sendText(String.valueOf(data[i]));
                        tempStream.sendText(senUnits[i]);
                    }
                }

            }
            //send closing sequence to the server
            tempStream.disconnect();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WebSocketException e) {
            e.printStackTrace();
        }

    }

    public boolean deleteArcFile() {

        mArchiveLog.closeFile();
        mIsArcLog = false;
        return (mArchiveLog.getOutputFile().delete());
    }

    public boolean deleteTmpFile() {

        mTempLog.closeFile();
        mIsTempLog = false;
        return (mTempLog.getOutputFile().delete());
    }

    public boolean deleteUnsFile() {

        mUnsentLog.closeFile();
        mIsUnsLog = false;
        return (mUnsentLog.getOutputFile().delete());
    }


    public boolean deleteAnalysisFile() {

        try {
            mAnalysisManager.mWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mAnalysisManager.mLogging = false;
        return (mAnalysisManager.mOutPut.delete());
    }

    //returns a set of sensors currently being held in the buffer map
    public Set<String> getSensors() {

        return mSenBufferMap.keySet();
    }

    /**
     * returns the array of sensor names that currently have data stored in a sensorBuffer
     * @return
     */
    /* //replaced by getSensors which returns a Set<String> instead of a String[]
    public String[] getSensorNames() {
        String[] sensorNames = new String[mSenBufferMap.size()];
        Set mSet = mSenBufferMap.keySet();
        int j = 0;
        for (Object s : mSet) {
            sensorNames[j] = (String) s;
            j++;
        }
        return sensorNames;
    }
    */

    //**********************************************************************************************
    //* Helper Functions
    //**********************************************************************************************

    /**
     * Retrieve data from the logged file and return an array of values
     * Only Retreives Calibrated DATA
     */
    private HashMap<String, double[]> retrieveData(Logging logger) {

        //find amount of repeats
            //int duplicate = 0;
            //ArrayList<String> alreadyChecked = new ArrayList<String>();
            //for (int i = 0; i < mLogger.mSensorNames.length; i++) {
            //    String tempName = mLogger.mSensorNames[i];
            //    for (int j = i +1; j < mLogger.mSensorNames.length; j++) {
            //        if (tempName == mLogger.mSensorNames[j] && !alreadyChecked.contains(tempName))
            //            duplicate++;
            //    }
            //    alreadyChecked.add(tempName);
            //}

        HashMap<String, double[]> data = new HashMap<String, double[]>();
        String[] senNames = logger.mSensorNames;

        //find number of sensors with calibrated data
            //int calCount = 0;
            //for (int i = 0; i < logger.mSensorFormats.length; i++) {
            //    if (logger.mSensorFormats[i] == "CAL") {
            //        calCount++;
            //        data.put(senNames[i], new double[logger.mDataSize]);
            //    }
            //}

        //get data like in the log and stream
        try {

            File outputFile = logger.getOutputFile();
            BufferedReader reader = new BufferedReader(new FileReader(outputFile));
            String line = null;
            String[] senUnits = logger.mSensorUnits;
            line = reader.readLine();

            //get to the data
            while ((!line.startsWith("START_DATA_LOG") && (line != null))) {line = reader.readLine();}

            int dataCount = 0;
            while ((line = reader.readLine()) != null && dataCount < logger.mDataSize) {
                //take apart the string
                double[] tempData = fromLineToData(line, logger);
                String[] formats  = logger.mSensorFormats;

                for (int i = 0; i < tempData.length; i++) {
                    if (senNames.length <= i || senUnits.length <= i ) continue;
                    //only want calibrated data
                    if (formats[i] != "CAL") continue;
                    data.get(senNames[i])[dataCount] = tempData[i];
                }
                dataCount++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    /**
     * Connect to the server.
     */
    private WebSocket connect() throws IOException, WebSocketException {

        return new WebSocketFactory()
                .setConnectionTimeout(TIMEOUT)
                .createSocket(SERVER)
                .addListener(new WebSocketAdapter() {
                    // A text message arrived from the server.
                    public void onTextMessage(WebSocket websocket, String message) {
                        //serverMsg = message;
                        // TextView Counter = (TextView) findViewById(R.id.connectionCount);
                        // Counter.setText(message);
                    }
                })
                .addExtension(WebSocketExtension.PERMESSAGE_DEFLATE)
                .connectAsynchronously();
    }

    //convert the system time in miliseconds to a "readable" date format with the next format: YYYY MM DD HH MM SS
    public static String fromMilisecToDate(long miliseconds){

        String date="";
        Date dateToParse = new Date(miliseconds);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        date = dateFormat.format(dateToParse);

        return date;
    }

    private double[] fromLineToData(String line, Logging logger) {

        int lineLength = logger.mSensorNames.length;

        String[] dataFrag;
        dataFrag = line.split(logger.mDelimiter, lineLength);
        double[] data = new double[dataFrag.length];

        for (int i = 0; i < dataFrag.length; i++)
            data[i] = parseDouble(dataFrag[i]);

        return data;
    }

    //check internet connection
    public boolean isInternetAvailable() {

        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }
    }

    public void closeWebSocket(WebSocket wbSoc) {

        //send closing sequence to server
        if (wbSoc != null && wbSoc.isOpen())
            wbSoc.disconnect();
    }

    public HashMap<String, double[]> getData(Logging logger) {
        return this.retrieveData(logger);
    }

    class streamStoredThread implements Runnable {
        Logging mLogger;

        public streamStoredThread (Logging logger) {
            mLogger = logger;
        }
        /*
         * Defines the code to run for this task.
         */
        @Override
        public void run() {
            // Moves the current Thread into the background
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            startStreamingStoredData(mLogger);
            deleteGenFile(mLogger);
        }
    }

    public boolean deleteGenFile(Logging logger) {

        logger.closeFile();
        return (logger.getOutputFile().delete());
    }

    //**********************************************************************************************
    //* Event Detection Functions
    //**********************************************************************************************
    /**
     *
     * @param accel
     * @return
     */
    private static boolean accelTooFast(double accel) {

        if (accel > 10.0 || accel < -10.0)
            return true;
        else
            return false;
    }

    //**********************************************************************************************
    //***** Buffer class to store most recent 100 data points
    //**********************************************************************************************

    //buffers for each sensor type created upon receiving an object cluster
    public class sensorBuffer {

        static final int BUFFSIZE = 100;
        String       mName;       //sensor name
        double[]     mSenData; //stores data values from the sensors
        double[]     mSenTime;
        int          mNextOpen;       //marks the nextAvaliable poisition in the buffer
        String       mFormat;
        String       mUnits;
        Boolean      mFull = false;
        Boolean      mEventDetection = false;
        List<String> mEventList = new ArrayList<String>();

        /**
         * Construcor for sensorBuffer
         * initialize array for data storage
         * @param name sensor name specified by the Shimmer 3 manual
         * @param format
         * @param units
         */
        public sensorBuffer(String name, String format, String units) {

            mName   = name;
            mFormat = format;
            mUnits  = units;
            mNextOpen = 0;
            mSenData = new double[BUFFSIZE];
            mSenTime = new double[BUFFSIZE];
        }

        /**
         * function addData called in the ProcessData implementation when a new data message from the Shimmer has been received
         * checks to make sure the data is for the appropriate sensor and format
         * @param name
         * @param format
         * @param data
         * @param timeStamp
         */
        public void addData(String name, String format, double data, double timeStamp){

            if (mName == name && mFormat == format){
                mSenData[mNextOpen % BUFFSIZE] = data;
                mSenTime[mNextOpen % BUFFSIZE] = timeStamp;

                if (mNextOpen == 99) mFull = true;
                mNextOpen = (mNextOpen + 1) % BUFFSIZE;
            }
        }

        /**
         * function getData returns an array of data from a specified point of a specified position
         * @param name
         * @param format
         * @param from
         * @param size
         * @return
         */
        public double[] getData(String name, String format, int from, int size) {

            double[] temp = new double[size];
            if (mName == name && mFormat == format){

                for (int i = 0, j = from; i < size; i++){
                    temp[i] = mSenData[j%BUFFSIZE];
                    j = j + 1 % BUFFSIZE;
                }
            }
            return temp;
        }

        /**
         * function getData returns an array of time stamps from a specified point of a specified position
         * @param name
         * @param format
         * @param from
         * @param size
         * @return
         */
        public double[] getTime(String name, String format, int from, int size) {

            double[] temp = new double[size];
            if (mName == name && mFormat == format){

                for (int i = 0, j = from; i < size; i++){
                    temp[i] = mSenTime[j%BUFFSIZE];
                    j = j + 1 % BUFFSIZE;
                }
            }
            return temp;
        }

        public void setEventList(String[] eventList) {

            for (int i = 0; i < mEventList.size(); i++)
                mEventList.add(eventList[i]);

            if (!mEventList.isEmpty())
                mEventDetection = true;
        }
    }

}
