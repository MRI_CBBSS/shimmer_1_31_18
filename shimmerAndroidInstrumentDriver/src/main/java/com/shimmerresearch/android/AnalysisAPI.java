package com.shimmerresearch.android;

/**
 * Created by josephreeves on 2/21/18.
 */

public abstract class AnalysisAPI {
    double mSamplingRate = 0;
    double mStartTime    = 0;

    public AnalysisAPI(double samplingRate, double startTime) {
        mSamplingRate = samplingRate;
        mStartTime = startTime;
    }

    abstract public double[] runAnalysis(double[] data);

    //if the analysis function requires an array of timesteps to match each data point, this will formulate those times
    public double[] getTimeSeries(int size) {
        double[] time = new double[size];
        double current = mStartTime;
        for (int i = 0; i < size; i++) {
            time[i] = current;
            current += mSamplingRate;
        }
        return time;
    }
}
