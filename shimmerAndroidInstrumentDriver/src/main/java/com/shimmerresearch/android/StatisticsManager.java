package com.shimmerresearch.android;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static com.shimmerresearch.android.ShimmerAdvance.IQR;
import static com.shimmerresearch.android.ShimmerAdvance.MAX;
import static com.shimmerresearch.android.ShimmerAdvance.MEAN;
import static com.shimmerresearch.android.ShimmerAdvance.MEDIAN;
import static com.shimmerresearch.android.ShimmerAdvance.MIN;
import static com.shimmerresearch.android.ShimmerAdvance.STANDARD_DEVIATION;
import static com.shimmerresearch.android.ShimmerAdvance.VARIANCE;

/**
 * Created by josephreeves on 2/13/18.
 */

//API to organize all statistics algorithms into classes with similar member variables and functions

public class StatisticsManager {
    //member variables
    //String mName;
    ArrayList<String> mStats;

    //static int    mType;

    /**
     * Constructor
     *
     */
    public StatisticsManager(ArrayList<String> stats) {
        mStats = stats;
    }

    //run pre-set statistics on input data
    HashMap<String, Double> runStatistic(double[] data, int first) {
        if (mStats == null) return null;

        //reorder data buffer so it is sequential
        double[] seqData = new double[data.length];
        int counter = first;
        for (int i = 0; i < data.length; i++) {
            seqData[i] = data[counter];
            counter = (counter + 1) % data.length;
        }

        HashMap<String, Double> statResults = new HashMap<>();

        for (int i = 0; i < mStats.size(); i++) {
            switch (mStats.get(i)) {
                case MEAN :
                    statResults.put(MEAN, Mean(seqData));
                    break;
                case VARIANCE :
                    statResults.put(VARIANCE, Variance(seqData));
                    break;
                case STANDARD_DEVIATION :
                    statResults.put(STANDARD_DEVIATION, Std(seqData));
                    break;
                case MEDIAN :
                    statResults.put(MEDIAN, Median(seqData));
                    break;
                case IQR :
                    statResults.put(IQR, IQR(seqData));
                    break;
                case MAX :
                    statResults.put(MAX, Max(seqData));
                    break;
                case MIN :
                    statResults.put(MIN, Min(seqData));
                    break;
                default :
                    break;
                //add statistics here
            }
        }

        return statResults;
    }

    //*************************************************************
    //Statistics Implementations, with data buffer input

    public static Double Mean(double[] data){
        Integer size = data.length;


        Double sum = 0.0;
        for(int i=0; i<=size-1; i++){
            sum = sum +data[i];
        }
        Double m = sum/size;

        return m;
    }

    public static Double Variance(double[] data){
        Integer size = data.length;

        Double mean =Mean(data);
        Double sumTemp =0.0;

        for(int i=0; i<=size-1; i++){
            sumTemp=sumTemp+ ((data[i]-mean)*(data[i]-mean));
        }

        Double var =sumTemp/size;
        return var;
    }


    public static Double Std(double[] data){
        return Math.sqrt(Variance(data));
    }

    public static Double Median(double[] data){
        Integer size = data.length;

        Arrays.sort(data);

        if (data.length % 2 == 0){
            return (double) (data[(size/2) - 1] + data[(size/2)]) / 2.0;
        }

        return data[(int) Math.floor(size/2)];
    }

    public static Double Quantiles(double[] data, Double dp){
        Integer size = data.length;

        Arrays.sort(data);
        Double[] quantiles=new Double[size];

        int index=-1;
        Double q=0.0;

        for(int i=0; i<=size-1; i++){
            quantiles[i]=(i+1-0.5)/size;
            if(i>0){
                if(dp>= quantiles[i-1] && dp<=quantiles[i]){
                    index=i;
                    break;
                }
            }
        }


        if(index !=-1){

            if(index<=size-1){
                q= data[index-1] + (data[index]-data[index-1])*((dp-quantiles[index-1])/(quantiles[index]-quantiles[index-1]));
            }

        }
        else if((1-dp) <=(dp-0)){
            q=data[size-1];
        }
        else{
            q=data[0];
        }


        return q;
    }

    public static Double IQR(double[] data){
        return Quantiles( data, .75)-Quantiles( data, .25);
    }

    public static Double Max(double[] data){
        return Quantiles( data, 1.0);
    }

    public static Double Min(double[] data){
        return Quantiles( data, 0.0);
    }


}
