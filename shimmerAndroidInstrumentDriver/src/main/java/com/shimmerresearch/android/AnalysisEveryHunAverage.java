package com.shimmerresearch.android;

/**
 * Created by josephreeves on 2/21/18.
 */

public class AnalysisEveryHunAverage extends AnalysisAPI {

    public AnalysisEveryHunAverage(double samplingRate, double startTime) {
        super(samplingRate, startTime);
    }

    @Override
    public double[] runAnalysis(double[] data) {
        double[] average = new double[data.length/100];

        for (int i = 0; i < average.length; i++) {
            double ave = 0;
            for (int j = 0; j < 100; j++) {
                ave += data[j + (i*100)];
            }
            average[i] = ave/100;
        }
        return average;
    }
}
