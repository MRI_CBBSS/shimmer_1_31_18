package com.shimmerresearch.android;

import android.os.Environment;
import android.widget.Switch;

import com.shimmerresearch.tools.Logging;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by josephreeves on 2/21/18.
 */

public class AnalysisManager {

    HashMap<String, String[]> mAnalysisMap;
    public File mOutPut;
    public BufferedWriter mWriter;
    String mFolderName = "Analysis";
    String mFileName   = null;
    boolean mFirstWrite = true;
    public boolean mLogging    = true;
    double  mSamplingRate = 0;


    public AnalysisManager(HashMap<String, String[]> anaList, double samplingRate) {

        mAnalysisMap = anaList;
        mSamplingRate = samplingRate;
    }

    public HashMap<String, double[][]> getAnalysisResults (ShimmerAdvance shimmer) {

        HashMap<String, double[][]> resultsMap = new HashMap<>();
        HashMap<String, double[]> senData    = shimmer.getData(shimmer.mArchiveLog);
        double startTime = shimmer.mStartTime;

        //preform analysis and return results
        for (String Key: mAnalysisMap.keySet()) {
            if (senData.get(Key) == null) continue;
            resultsMap.put(Key, new double[mAnalysisMap.get(Key).length][]);
            for (int j = 0; j < mAnalysisMap.get(Key).length; j++) {
                String analName = mAnalysisMap.get(Key)[j];
                switch (analName) {
                    case ShimmerAdvance.EVERY_HUN_AVERAGE:
                        AnalysisAPI temp = new AnalysisEveryHunAverage(mSamplingRate, startTime);
                        resultsMap.get(Key)[j] = temp.runAnalysis(senData.get(Key));
                        break;
                }
            }
        }

        //*************************
        //write Analysis to a file
        //*************************
        if (mFirstWrite) {
            File root = new File(Environment.getExternalStorageDirectory() + "/" + mFolderName);

            if (!root.exists()) {
                if (root.mkdir()) ; //directory is created;
            }
            //changed to text file as opposed to .dat
            // outputFile = new File(root, mFileName+".dat");
            mFileName = ShimmerAdvance.fromMilisecToDate(System.currentTimeMillis()) + "Analysis" + shimmer.getBluetoothAddress();

            mOutPut = new File(root, mFileName + ".txt");
            mFirstWrite = false;
        }

        if (mLogging) {
            //log analysis
            try {
                mWriter = new BufferedWriter(new FileWriter(mOutPut));
                //preform analysis and return results
                for (String Key : mAnalysisMap.keySet()) {
                    mWriter.write(Key);
                    mWriter.newLine();
                    for (int j = 0; j < mAnalysisMap.get(Key).length; j++) {
                        String analName = mAnalysisMap.get(Key)[j];
                        switch (analName) {
                            case ShimmerAdvance.EVERY_HUN_AVERAGE:
                                mWriter.write(ShimmerAdvance.EVERY_HUN_AVERAGE);
                                mWriter.newLine();
                                for (int z = 0; z < resultsMap.get(Key)[j].length; z++) {
                                    mWriter.write(resultsMap.get(Key)[j][z] + ",");
                                }
                                break;
                        }
                        mWriter.newLine();
                    }
                    mWriter.newLine();
                }
                mWriter.close();

            } catch (IOException e) {

            }
        }

        return resultsMap;
    }

}
